![xcell](Applications/Logo/logo.png)
# xcell: tools for simulating the extracellular domain

Framework for modelling the extracellular space in neural simulations. Detailed code documentation [to come...](https://cbcgirard.github.io/xcell)