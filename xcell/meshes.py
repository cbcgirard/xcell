#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Mesh topology
"""
import numpy as np
import numba as nb
from . import elements
from . import util
from . import fem
from . import geometry as geo
from tqdm import trange


class Mesh:
    def __init__(self, bbox, elementType='Admittance'):
        self.bbox = bbox
        self.extents = (bbox[3:]-bbox[:3])/2
        self.span = bbox[3:]-bbox[:3]
        self.center = bbox[:3]+self.span/2
        self.elements = []
        self.conductances = []
        self.elementType = elementType
        self.nodeCoords = np.empty((0, 3), dtype=np.float64)
        self.edges = []

        self.minl0 = 0

        self.indexMap = []
        self.inverseIdxMap = {}
        self.boundaryNodes = np.empty(0, dtype=np.int64)

    def __getstate__(self):

        state = self.__dict__.copy()

        elInfo = []
        for el in self.elements:
            d = {'origin': el.origin,
                 'span': el.span,
                 'l0': el.l0,
                 'sigma': el.sigma,
                 'vertices': el.vertices,
                 'faces': el.faces,
                 'index': el.index}
            elInfo.append(d)

        state['elements'] = elInfo
        state['inverseIdxMap'] = {}
        if 'tree' in state:
            dtree = {'origin': self.tree.origin,
                     'span': self.tree.span,
                     'sigma': self.tree.sigma,
                     'index': self.tree.index
                     }
            state['tree'] = dtree
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        elDicts = self.elements.copy()

        self.elements = []

        for ii, el in enumerate(elDicts):
            self.addElement(el['origin'], el['span'],
                            el['sigma'], el['index'])
            self.elements[-1].faces = el['faces']
            self.elements[-1].vertices = el['vertices']

        self.inverseIdxMap = util.getIndexDict(self.indexMap)

        if 'tree' in state:
            treeDict = self.tree.copy()
            tree = Octant(origin=treeDict['origin'],
                          span=treeDict['span'],
                          sigma=treeDict['sigma'],
                          index=treeDict['index'])
            tree._Octant__recreateTree(self.elements)
            self.tree = tree

    def getContainingElement(self, coords):
        """
        Get element containing specified point.

        Parameters
        ----------
        coords : float[:]
            Cartesian coordinates of point.

        Raises
        ------
        ValueError
            Error if no element contains the point.

        Returns
        -------
        elem : TYPE
            Containing element.

        """
        nElem = len(self.elements)

        # TODO: workaround fudge factor
        tol = 1e-9*np.ones(3)

        for nn in nb.prange(nElem):
            elem = self.elements[nn]
            # if type(elem) is dict:
            #     delta=coords-elem['origin']
            #     ext=elem['extents']
            # else:
            delta = coords-elem.origin+tol
            ext = elem.span+2*tol

            difs = np.logical_and(delta >= 0, delta <= ext)
            if all(difs):
                return elem

        raise ValueError('Point (%s) not inside any element' %
                         ','.join(map(str, coords)))

    def finalize(self):
        pass

    def addElement(self, origin, span, sigma, index):
        """
        Insert element into the mesh.

        Parameters
        ----------
        origin : float[:]
            Cartesian coords of element's origin.
        extents : float[:]
            Length of edges in x,y,z.
        sigma : float
            Conductivity of element.
        nodeIndices : int64[:]
            Numbering of element nodes according to global mesh.

        Returns
        -------
        None.

        """

        newEl = Octant(origin, span, sigma=sigma, index=index)
        self.elements.append(newEl)

    def getConductances(self, elements=None):
        """
        Get the discrete conductances from every element.


        Parameters
        ----------
        elements : list of `~xcell.mesh.Octant`, optional
            DESCRIPTION. The default is None, which uses all in mesh.

        Returns
        -------
        edgeIndices : int64[:,:]
            List of node pairs spanned by each conductance.
        conductances : float
            Conductance in siemens.
        transforms : list of ints
            Substitutions for graph-dual meshes.

        """

        if elements is None:
            elements = self.elements
        nElem = len(elements)

        clist = []
        elist = []
        transforms = []

        for nn in nb.prange(nElem):

            elem = elements[nn]

            if self.elementType == 'Admittance':
                elConds = fem.getAdmittanceConductances(elem.span, elem.sigma)
                elEdges = elem.vertices[fem.ADMITTANCE_EDGES]
            elif self.elementType == 'FEM':
                elConds = fem.getHexConductances(elem.span, elem.sigma)
                elEdges = elem.vertices[fem.getHexIndices()]
            elif self.elementType == 'Face':
                rawCond = fem.getFaceConductances(elem.span, elem.sigma)
                rawEdge = elem.faces[fem.FACE_EDGES]

                elConds = []
                elEdges = []
                for ii in nb.prange(6):
                    neighbors = elem.neighbors[ii]
                    nNei = len(neighbors)

                    if nNei <= 1:
                        elConds.append(rawCond[ii])
                        elEdges.append(rawEdge[ii])

                    else:
                        # faces are shared; split original edge
                        origNode = rawEdge[ii, 0]
                        xform = []

                        neiNodeIdx = ii+(-1)**ii

                        for jj in nb.prange(nNei):
                            elConds.append(rawCond[ii]/nNei)

                            neighbor = neighbors[jj]
                            neiNode = neighbor.faces[neiNodeIdx]

                            edge = np.array([neiNode,
                                             rawEdge[ii, 1]])
                            elEdges.append(edge)
                            xform.append(neiNode)

                        xform.append(origNode)
                        transforms.append(xform)

            elist.extend(elEdges)
            clist.extend(elConds)

        edgeIndices = np.array(elist)
        conductances = np.array(clist)

        return edgeIndices, conductances, transforms

    def getL0Min(self):
        """
        Get the smallest edge length in mesh

        Returns
        -------
        l0Min : float
            smallest edge length.

        """
        l0Min = np.infty

        for el in self.elements:
            l0Min = min(l0Min, el.l0)
        return l0Min

    def getBoundaryNodes(self):
        """
        Get the indices of nodes on the domain boundary.

        Returns
        -------
        globalIndices : int[:]
            Indices of current mesh nodes at periphery.

        """
        mins, maxes = np.hsplit(self.bbox, 2)

        atmin = np.equal(mins, self.nodeCoords)
        atmax = np.equal(maxes, self.nodeCoords)
        isbnd = np.any(np.logical_or(atmin, atmax), axis=1)
        globalIndices = np.nonzero(isbnd)[0]

        return globalIndices

    def getIntersectingelements(self, axis, coordinate):
        """
        Find elements intersected by a cartesian plane.

        Parameters
        ----------
        axis : int
            Normal of intersecting plane (0->x, 1->y,2->z).
        coordinate : float
            Coordinate of plane alonx normal axis.

        Returns
        -------
        elements : list of `~xcell.meshes.Octant`
            Elements intersected by the plane.

        """
        elements = []

        for el in self.elements:
            gt = el.origin[axis] >= coordinate
            lt = (el.origin[axis]-el.span[axis]) < coordinate
            if gt and lt:
                elements.append(el)

        return elements


class Octree(Mesh):
    def __init__(self, boundingBox, maxDepth=10, elementType='Admittance'):
        self.center = np.mean(boundingBox.reshape(2, 3), axis=0)
        self.span = (boundingBox[3:]-boundingBox[:3])
        super().__init__(boundingBox, elementType)

        self.maxDepth = maxDepth
        self.bbox = boundingBox
        self.indexMap = np.empty(0, dtype=np.uint64)
        self.inverseIdxMap = {}

        self.changed = True

        # coord0=self.center-self.span/2

        self.tree = Octant(origin=boundingBox[:3],
                           span=self.span)

    def getContainingElement(self, coords):

        el = self.tree.getContainingElement(coords)
        return el

    def getIntersectingelements(self, axis, coordinate):
        elements = self.tree.getIntersectingElement(axis, coordinate)
        return elements

    def refineByMetric(self, minl0Function, refPts, maxl0Function=None, coefs=None, coarsen=True):
        """
        Recursively splits elements until l0Function evaluated at the center
        of each element is greater than that element's l0'

        Parameters
        ----------
        minl0Function : function
            Function returning a scalar for each input cartesian coordinate.
        refPts : float[:,3]
            Cartesian coordinates where distance is evaluated from.
        maxl0Function : function or None, optional
            Function giving maximum l0 for coarsening. 
            The default is None, which uses minl0Function.
        coefs : float[:], optional
            Factor multiplied each candidate l0. The default is None.
        coarsen : bool, optional
            Whether to prune elements larger than target l0. The default is True.

        Returns
        -------
        changed : bool
            Adaptation resulted in new topology

        """

        if maxl0Function is None:
            maxl0Function = minl0Function

        changed = self.tree.refineByMetric(
            minl0Function, refPts, self.maxDepth, coefs)

        if coarsen:
            pruned, _ = self.tree.coarsenByMetric(
                maxl0Function, refPts, self.maxDepth, coefs)

            # if pruned:
            #     print()
            #     changed|=pruned

            self.changed = changed | pruned

        return changed

    def finalize(self):
        """
        Convert terminal octants to mesh elements, mapping sparse to dense numbering.

        Returns
        -------
        None.

        """
        octs = self.tree.getTerminalOctants()

        self.elements = octs

        if self.elementType == 'Face':
            self.getElementAdjacencies()

        self.changed = False

        return

    def printStructure(self):
        """
        Debug tool to print structure of tree

        Returns
        -------
        None.

        """
        self.tree.printStructure()

    def octantByList(self, indexList, octant=None):
        """
        Select an octant by recursing through a list of indices.

        Parameters
        ----------
        indexList : int64[]
            Octant identifier, where i=indexList[n] specifies child i of octant n
        octant : Octant, optional
            Used internally to recurse. The default is None.

        Returns
        -------
        Octant
            The octant object specified.

        """
        head = indexList.pop(0)

        if octant is None:
            octant = self.tree
        oc = octant.children[head]
        if len(oc.children) == 0:
            # terminal octant reached, match found
            return [oc]
        else:
            if len(indexList) == 0:
                # list exhausted at non-terminal octant
                return oc.children
            else:
                # continue recursion
                return self.octantByList(indexList, oc)

    def countelements(self):
        """
        Get number of terminal elements in tree.

        Returns
        -------
        int
            Number of leaf elements.

        """
        return self.tree.countelements()

    def getCoordsRecursively(self):
        """
        Determine coordinates of mesh nodes.

        Recurses through `Octant.getCoordsRecursively`

        Returns
        -------
        coords : 2d float array
            Cartesian coordinates of each mesh node.

        """

        # TODO: parallelize?
        # around 2x element creation time (not bad)
        # rest of function very fast

        asdual = self.elementType == 'Face'
        i = self.tree.getCoordsRecursively(asDual=asdual)

        indices = np.unique(np.array(i, dtype=np.uint64))
        self.indexMap = indices

        # c=util.indexToCoords(indices,self.span,self.maxDepth+int(asdual))
        # coords=c+self.bbox[:3]
        coords = util.indexToCoords(indices, self.bbox[:3], self.span)

        return coords

    def getBoundaryNodes(self):
        """
        Get the indices of nodes on the domain boundary.

        Returns
        -------
        globalIndices : int[:]
            Indices of current mesh nodes at periphery.

        """
        bnodes = []

        for ii in nb.prange(self.indexMap.shape[0]):
            nn = self.indexMap[ii]
            xyz = util.index2pos(nn, util.MAXPT)
            if np.any(xyz == 0) or np.any(xyz == util.MAXPT-1):
                bnodes.append(ii)

        return np.array(bnodes)

# TODO: marked for deletion
    # def getDualMesh(self):
    #     octs = self.tree.getTerminalOctants()
    #     self.elements = octs
    #     numel = len(self.elements)

    #     coords = np.empty((numel, 3), dtype=np.float64)
    #     edges = []
    #     conductances = []
    #     nodeIdx = []
    #     bnodes = []

    #     temp = []
    #     for ii in nb.prange(numel):
    #         el = self.elements[ii]

    #         elIndex = util.octantListToIndex(np.array(el.index),
    #                                          self.maxDepth)
    #         el.globalNodeIndices[0] = elIndex
    #         coords[ii] = el.center
    #         nodeIdx.append(elIndex)

    #         gEl = fem.getFaceConductances(el.span, el.sigma)

    #         neighborList = util.octantNeighborIndexLists(np.array(el.index))

    #         isBnd = False

    #         for step in nb.prange(6):
    #             neighborI = neighborList[step]
    #             if len(neighborI) == 0:
    #                 isBnd = True
    #             else:
    #                 #     neighborI=el.getNeighborIndex(step)
    #                 # if neighborI is not None:
    #                 # neighbor exists
    #                 tstNeighbor = self.octantByList(neighborI)
    #                 nNeighbors = len(tstNeighbor)

    #                 if nNeighbors > 1:
    #                     ax = step//2
    #                     dr = step % 2
    #                     neighbors = [n for n in tstNeighbor if util.toBitArray(
    #                         n.index[-1])[ax] ^ dr]
    #                     nNeighbors = len(neighbors)
    #                 else:
    #                     neighbors = tstNeighbor

    #                 for neighbor in neighbors:
    #                     # neighborIndex=neighbor.globalNodeIndices[0]
    #                     neighborIndex = util.octantListToIndex(np.array(neighbor.index),
    #                                                            self.maxDepth)

    #                     gA = fem.getFaceConductances(neighbor.span,
    #                                                  neighbor.sigma)[step//2]
    #                     gB = gEl[step//2]/nNeighbors

    #                     gnet = 0.5*(gA*gB)/(gA+gB)

    #                     conductances.append(gnet)
    #                     edges.append([elIndex, neighborIndex])

    #         if isBnd:
    #             bnodes.append(elIndex)

    #     idxMap = np.array(nodeIdx)

    #     corrEdges = util.renumberIndices(np.array(edges), idxMap)
    #     self.boundaryNodes = util.renumberIndices(np.array(bnodes, ndmin=2),
    #                                               idxMap).squeeze()

    #     return coords, idxMap, corrEdges, np.array(conductances)

    def getElementAdjacencies(self):
        """
        Get the neighboring elements.

        Returns
        -------
        adjacencies : list of elements
            Adjacent elements in order of (+x,-x, +y,-y, +z,-z).

        """
        numel = len(self.elements)

        adjacencies = []

        for ii in trange(numel, desc='Calculating adjacency'):
            el = self.elements[ii]

            maybeNeighbors = util.octantNeighborIndexLists(np.array(el.index))
            neighborSet = []
            for nn in nb.prange(6):
                nei = maybeNeighbors[nn]
                if len(nei) > 0:
                    tstEl = self.octantByList(nei)

                    if len(tstEl) > 1:
                        ax = nn//2
                        dr = nn % 2

                        # select elements on opposite side of
                        # sameFace=[((n>>ax)&1) for n in range(6)]
                        # neighbors=[tstEl[n] for n in sameFace]

                        neighbors = [n for n in tstEl if util.toBitArray(
                            n.index[-1])[ax] ^ dr]
                        # neighbors=[n for n in tstEl if util.toBitArray(n.index[-1])[ax]]

                    else:
                        neighbors = tstEl

                else:
                    neighbors = []

                neighborSet.append(neighbors)

            el.neighbors = neighborSet
            adjacencies.append(neighborSet)

        return adjacencies


class Octant():
    def __init__(self, origin, span, depth=0, sigma=np.ones(3), index=[], oXYZ=np.zeros(3, dtype=np.int32)):
        self.origin = origin
        self.span = span
        self.center = origin+span/2
        self.bbox = np.concatenate((origin, origin + span))
        self.l0 = np.prod(span)**(1/3)

        self.children = []
        self.depth = depth
        self.index = index

        # don't calculate indices here, since we might split later
        self.vertices = []
        self.faces = []

        self.sigma = sigma

        self.neighbors = 6*[[]]
        self.oXYZ = oXYZ

    def __recreateTree(self, elements):
        childLists = []
        D = self.depth
        for ind in range(8):
            childList = [el for el in elements if el.index[D] == ind]
            childLists.append(childList)

        self.split()
        for ii, ch in enumerate(self.children):
            clist = childLists[ii]
            if len(clist) == 1:
                el = clist[0]
                el.depth = len(el.index)
                self.children[ii] = el
            else:
                ch.__recreateTree(clist)

    def countelements(self):
        """
        Return the number of leaf elements contained within octant.

        Returns
        -------
        int
            Number of leaf elements within.

        """
        if len(self.children) == 0:
            return 1
        else:
            return sum([ch.countelements() for ch in self.children])

    # @nb.njit(parallel=True)
    def split(self, division=np.array([0.5, 0.5, 0.5])):
        """
        Split element into its child octants.

        Parameters
        ----------
        division : float[3], optional
            Fraction of division in x,y,z directions. The default is np.array([0.5, 0.5, 0.5]).

        Returns
        -------
        None.

        """
        newSpan = self.span*division

        for ii in nb.prange(8):
            offset = newSpan*util.OCT_INDEX_BITS[ii]
            # offset=util.toBitArray(ii)*newSpan
            newOrigin = self.origin+offset
            newIndex = self.index.copy()
            newIndex.append(ii)

            self.children.append(Octant(origin=newOrigin,
                                        span=newSpan,
                                        depth=self.depth+1,
                                        index=newIndex))

    def getCoordsRecursively(self, asDual=False):
        """
        Get the coordinates of the mesh within the element.

        Parameters
        ----------
        asDual : bool, optional
            Whether to return the mesh-dual nodes instead of vertices. The default is False.

        Returns
        -------
        indices : list of int
            Indices of node according to universal numbering.

        """
        if len(self.children) == 0:

            if asDual:
                indices = self.faces.tolist()
            else:
                indices = self.vertices.tolist()
        else:
            indices = []

            for ch in self.children:
                i = ch.getCoordsRecursively(asDual=asDual)
                indices.extend(i)

        return indices

    # @nb.njit(parallel=True)
    def refineByMetric(self, l0Function, refPts, maxDepth, coefs):
        """
        Recursively splits elements until l0Function evaluated at the center
        of each element is greater than that element's l0'

        Parameters
        ----------
        minl0Function : function
            Function returning a scalar for each input cartesian coordinate.
        refPts : float[:,3]
            Cartesian coordinates where distance is evaluated from.
        maxDepth : int
            Maximum depth of splitting permitted
        coefs : float[:], optional
            Factor multiplied each candidate l0. The default is None.

        Returns
        -------
        changed : bool
            Adaptation resulted in new topology

        """
        changed = False
        l0Target, whichPts = util.reduceFunctions(
            l0Function, refPts, self.bbox, coefs=coefs)
        filt = np.logical_and(whichPts, maxDepth > self.depth)
        nextPts = refPts[filt]
        nextMaxDepths = maxDepth[filt]

        if coefs is not None:
            nextCoefs = coefs[filt]
        else:
            nextCoefs = coefs

        if nextPts.shape[0] > 0:  # and self.depth<maxDepth:
            if len(self.children) == 0:
                changed = True
                self.split()
            for ii in nb.prange(8):
                changed |= self.children[ii].refineByMetric(l0Function,
                                                            nextPts,
                                                            nextMaxDepths, nextCoefs)

        return changed

    def coarsenByMetric(self, metric, refPts, maxdepth, coefs):
        """
        Delete children if element and all children are smaller than target.

        Parameters
        ----------
        metric : function
            Function returning a scalar for each input cartesian coordinate.
        refPts : float[:,3]
            Cartesian coordinates where distance is evaluated from.
        maxdepth : int
            Maximum depth of splitting.
        coefs : float[:]
            Factor multiplied each candidate l0.

        Returns
        -------
        changed : bool
            Whether mesh topology was altered.
        undersized : bool
            Whether element is smaller than all targets.

        """
        changed = False
        _, whichPts = util.reduceFunctions(
            metric, refPts, self.bbox, coefs=coefs, returnUnder=False)

        # let metric implicitly prune if maxdepth lowered
        # causes insufficient meshing otherwise
        # undersize=np.all(whichPts) #or self.depth<maxdepth

        filt = np.logical_or(whichPts, maxdepth < self.depth)
        undersize = np.all(filt)

        if self.isTerminal():
            # end condition
            pass
        else:
            # recurse to end
            for ch in self.children:
                chChanged, chUnder = ch.coarsenByMetric(
                    metric, refPts, maxdepth, coefs)
                changed |= chChanged
                undersize &= chUnder

            if undersize:
                changed = True
                for ch in self.children:
                    del ch
                self.children = []
                self.calcIndices()

        return changed, undersize

# TODO: marked for deletion
    # def prune(self):
    #     for ch in self.children:
    #         ch.prune()
    #         del ch

    def printStructure(self):
        """
        Print out octree structure.

        Returns
        -------
        None.

        """
        base = '> '*self.depth
        print(base+str(self.l0))

        for ch in self.children:
            ch.printStructure()

    def isTerminal(self):
        """
        Determine if element is terminal (has no children)

        Returns
        -------
        terminal : bool
            True if element has no children.

        """
        terminal = len(self.children) == 0
        if terminal and len(self.vertices) == 0:
            self.calcIndices()
        return terminal

# TODO: marked for deletion
    # def containsPoint(self, coord):
    #     gt = np.greater_equal(coord, self.origin)
    #     lt = np.less_equal(coord-self.origin, self.span)
    #     return np.all(gt & lt)

    def intersectsPlane(self, axis, coord):
        cmin = self.origin[axis]
        cmax = cmin+self.span[axis]
        return (cmin <= coord) & (coord < cmax)

    def getTerminalOctants(self):
        """
        Get all childless octants.

        Returns
        -------
        list of Octants
            Childless octants (the actual elements of the mesh)

        """
        if self.isTerminal():
            return [self]
        else:

            descendants = []
            for ch in self.children:
                # if len(ch.children)==0:
                grandkids = ch.getTerminalOctants()

                if grandkids is not None:
                    descendants.extend(grandkids)
        return descendants

    def calcIndices(self):
        """
        Calculate the universal indices of the element's nodes.

        Returns
        -------
        None.

        """
        elList = np.array(self.index, dtype=np.int8)
        inds = util.indicesWithinOctant(elList, fem.HEX_POINT_INDICES)

        self.vertices = inds[:8]
        self.faces = inds[8:]

    def getContainingElement(self, coords):
        """
        Find the element that contains the specified point.

        Parameters
        ----------
        coords : float[3]
            Cartesian coordinates of test point.

        Returns
        -------
        xcell.meshes.Octant or None
            Element containing point.

        """
        if len(self.children) == 0:
            # if self.containsPoint(coords):
            if geo.isInBBox(self.bbox, coords):
                return self
            else:
                return None
        else:
            for ch in self.children:
                tmp = ch.getContainingElement(coords)
                if tmp is not None:
                    return tmp
            return None

    def getIntersectingElement(self, axis, coord):
        """
        Find elements intersected by a cartesian plane.

        Parameters
        ----------
        axis : int
            Normal of intersecting plane (0->x, 1->y,2->z).
        coord : float
            Coordinate of plane alonx normal axis.

        Returns
        -------
        elements : list of `~xcell.meshes.Octant`
            Elements intersected by the plane.

        """
        descendants = []
        if len(self.children) == 0:
            if self.intersectsPlane(axis, coord):
                return [self]
            else:
                return descendants
        else:

            for ch in self.children:
                intersects = ch.getIntersectingElement(axis, coord)
                if intersects is not None:
                    descendants.extend(intersects)

            return descendants

# TODO: Marked for deletion
    # def getNeighborIndex(self, direction):

    #     ownInd = np.array(self.index)

    #     return util.octantNeighborIndexList(ownInd, direction)

    def interpolateWithin(self, coordinates, values):
        """
        Interpolate values within the element from specified vertices/face nodes

        Parameters
        ----------
        coordinates : float[:,3]
            Cartesian coordinates to interpolate at (in global coordinate system).
        values : float[8] or float[7]
            Array of values at vertices (n=8) or faces (n=7).

        Returns
        -------
        interp : float[:]
            Interpolated values at the specified points.

        """
        coords = fem.toLocalCoords(coordinates, self.center, self.span)
        if values.shape[0] == 8:
            interp = fem.interpolateFromVerts(values, coords)
        else:
            interp = fem.interpolateFromFace(values, coords)

        return interp

    # TODO: fix hack of changing node indices
    def getUniversalVals(self, knownValues):

        # oldInds=self.globalNodeIndices

        allVals = np.empty(15, dtype=np.float64)
        allInds = np.empty(15, dtype=np.int64)

        indV = self.vertices
        indF = self.faces

        if knownValues.shape[0] == 8:
            vVert = knownValues
            vFace = fem.interpolateFromVerts(knownValues,
                                             fem.HEX_FACE_COORDS)
        else:
            vFace = knownValues
            vVert = fem.interpolateFromFace(knownValues,
                                            fem.HEX_VERTEX_COORDS)

        allInds = np.concatenate((indV, indF))
        allVals = np.concatenate((vVert, vFace))

        return allVals, allInds

    def getPlanarValues(self, globalValues, axis=2, coord=0.):
        zcoord = (coord-self.center[axis])/self.span[axis]

        inds = 3*[[-1., 1.]]
        inds[axis] = [zcoord]
        localCoords = np.array([[x, y, z] for z in inds[2]
                                for y in inds[1] for x in inds[0]])

        if globalValues.shape[0] == 8:
            planeVals = fem.interpolateFromVerts(globalValues, localCoords)
        else:
            planeVals = fem.interpolateFromFace(globalValues, localCoords)

        return planeVals
